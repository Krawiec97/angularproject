
---

## Angular -> Project 2

Call method in dialer.js

Krok 1 : Odpalamy konsolę linuxową, lub program imitujący typu cmder. Korzystając z komendy "cd" podajemy ścieżkę do której chcemy zapisać plik np: "cd Plik/następnyPlik" a następnie komendą "git clone https://Krawiec97@bitbucket.org/Krawiec97/node-angularproject.git" pobieramy projekt na swój komputer.

Krok 2: Wchodzimy do folderu głównego komendą "cd" i następnie instalujemy wszystkie potrzebne biblioteki korzystając z komendy "npm install".

Krok 3: Wpisujemy komendę "node dialer.js" (spodziewamy się komunikatu : "app listening on port 3000") (zostawiamy otwarty projekt)

Krok 4: Odpalamy konsolę linuxową, lub program imitujący typu cmder. Korzystając z komendy "cd" podajemy ścieżkę do której chcemy zapisać plik np: "cd Plik/następnyPlik" a następnie komendą "git clone https://Krawiec97@bitbucket.org/Krawiec97/node-angularproject.git" pobieramy projekt na swój komputer.

Krok 5: instalujemy Angulara globalnie komendą w cmder'ze : npm install -g @angular/cli. Potwierdzamy klikając enter

Krok 6: Wchodzimy do folderu głównego projektu front komendą "cd" i następnie instalujemy wszystkie potrzebne biblioteki korzystając z komendy "npm install".

Krok 7: Wpisujemy komendę "ng serve" (spodziewamy się komunikatu : " Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **")

Krok 8: Otwieramy przeglądarkę , wpisujemy localhost:4200, wpisujemy do okna poprawny numer i cieszymy się możliwością dzwonienia z okna przeglądarki ;)


---
